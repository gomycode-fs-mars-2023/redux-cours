import React, {useEffect, useState} from 'react';
import { useParams, useLocation } from 'react-router-dom';

const User = () => {

    const locations  = useLocation();
    const parms = useParams();
    const [email, setEmail] = useState('');
    const [description, setDescription] = useState('');

    useEffect(()=>{
        console.log(parms, locations);
    }, [])

    return (
        <div>
            User id is : {parms.id}   and name is: {parms.name}
            <input type="text" value={email} onChange={(e)=>setEmail(e.target.value)} />
            <input type="text" value={description} onChange={(e)=>setDescription(e.target.value)} />
        </div>
    );
}

export default User;
