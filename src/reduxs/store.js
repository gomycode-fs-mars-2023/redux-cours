import { configureStore } from '@reduxjs/toolkit'
import userSlice from './users/userSlice';
import productSlice from './products/productSlice';

export const store = configureStore({
  reducer: {
    user : userSlice,
    product: productSlice
  },
})

