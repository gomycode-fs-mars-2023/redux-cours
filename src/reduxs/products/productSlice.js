import { createSlice } from '@reduxjs/toolkit'

const initialState = [
    {
        id: "",
        name: "",
        price: 0,
        quantity: 0
    }
]

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    addProduct: (state, action) => {
        state.push(action.payload);
    },
    removeProduct: (state, action) => {
        const newState = state.filter(state => state.id != action.payload.id);
        state.push(newState);
    }
  },
})

// Action creators are generated for each case reducer function
export const { addUser, removeUser } = productSlice.actions

export default productSlice.reducer