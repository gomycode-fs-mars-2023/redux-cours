import { createSlice } from '@reduxjs/toolkit'

const initialState = [
    {
        id: "",
        firstName: "",
        lastName: "",
        password: ""
    }
]

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    addUser: (state, action) => {
        console.log(action)
        state.push(action.payload);
    },
    removeUser: (state, action) => {
        const newState = state.filter(state => state.id != action.payload.id);
        state.push(newState);
    }
  },
})

// Action creators are generated for each case reducer function
export const { addUser, removeUser } = userSlice.actions

export default userSlice.reducer
