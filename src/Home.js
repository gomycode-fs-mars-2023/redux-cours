import React, {useState} from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import { addUser, removeUser } from './reduxs/users/userSlice';

const Home = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch()

    const [user, setUser] = useState({
        id: "",
        firstName: "",
        lastName: "",
        password: ""
    });

    function handleChange(e){
        e.preventDefault();
        setUser({...user, [e.target.name]: e.target.value, id: (new Date()).getMilliseconds() })
    }

    function submit(e){
        e.preventDefault();
        dispatch(addUser(user))
        console.log(user);
    }
    const usersData = useSelector((state) => state.user);
    return (
        <div>
            <form action="" method="post">
                <input type="text" name='firstName' placeholder='firstName' onChange={handleChange} />
                <input type="text" name='lastName' placeholder='lastName' onChange={handleChange} />
                <input type="password" name='password' onChange={handleChange} />
                <input type="button" value="Enregistrer" onClick={submit}  />
            </form>
            <button onClick={()=>navigate('/contact')}>Aller</button>
            
            <div>
            {usersData && usersData.map((user, index) =>(<div key={index}>
                <span>{user.id}</span>
                <h3>{user.firstName}</h3>
                <h4>{user.lastName}</h4>
                <h5>{user.password}</h5>
                <hr />
            </div>))}
        </div>
        </div>
    );
}

export default Home;
