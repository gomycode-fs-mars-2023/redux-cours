import React from 'react';
import { useSelector } from 'react-redux'

const Contact = () => {

    const usersData = useSelector((state) => state.user);
    const productData = useSelector((state) => state.product);

    return (
        <div>
            {usersData && usersData.map((user, index) =>(<div key={index}>
                <span>{user.id}</span>
                <h3>{user.firstName}</h3>
                <h4>{user.lastName}</h4>
                <h5>{user.password}</h5>
                <hr />
            </div>))}
        </div>
    );
}

export default Contact;
